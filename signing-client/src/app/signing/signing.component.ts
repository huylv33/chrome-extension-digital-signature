import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-signing',
  templateUrl: './signing.component.html',
  styleUrls: ['./signing.component.scss']
})
export class SigningComponent implements OnInit {
  pdfSrc = "https://vadimdez.github.io/ng2-pdf-viewer/assets/pdf-test.pdf";
  isSuccess = false;
  constructor() {
    document.addEventListener("signResultEvent",(event) =>  {
      console.log(event);
      this.isSuccess = true;
    })
  }

  ngOnInit(): void {
  }

  onSign() {  
    // data you want to sent
    let data = {
      type: 'Some data',
      fileUrl: this.pdfSrc  
    };
    console.log(data);  
    document.dispatchEvent(new CustomEvent('signEvent', {detail: data}));
  }
}
