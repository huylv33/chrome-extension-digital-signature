import { Injectable, OnInit } from '@angular/core';
import { IDocument } from './model/document.model';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';


export type EntityResponseType = HttpResponse<IDocument>;
export type EntityArrayResponseType = HttpResponse<IDocument[]>;


@Injectable({
  providedIn: 'root'
})
export class DocumentService {
  private resourceUrl =  'http://localhost:8080/api/documents';

  constructor(protected http: HttpClient) { }

  query(req?: any): Observable<EntityArrayResponseType> {
    return this.http.get<IDocument[]>(this.resourceUrl,{observe: 'response' });
  }
  sign(obj:any,headers?:HttpHeaders):Observable<any> {
    return this.http.post<any>(this.resourceUrl + "/sign",obj,{headers:headers,observe:'response'});
  }
}
