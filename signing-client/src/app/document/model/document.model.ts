export interface IDocument {
    id ?: number,
    name ?: string | null,
    fileType ?: string | null,
    path ?:string | null,
    isSigned ?: number | null
}

export class Document implements IDocument {
    constructor(public id?: number, public name?: string | null,public fileType?: string | null,public path?: string | null,
        public isSign?: number | null){};
}

export function getDocumentIdentifier(document: IDocument): number | undefined {
    return document.id;
}
  
