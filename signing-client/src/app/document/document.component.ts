import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { DocumentService } from './document.service';
import { IDocument } from './model/document.model';

@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss']
})
export class DocumentComponent implements OnInit {
  documents?: IDocument[] | any;
  documentSelected: IDocument | any;
  constructor(protected documentService:DocumentService) { 
    let signToken = "";
    document.addEventListener("selectCertResultEvent",(event:any) => {
      console.log(event);
      let obj = {
        id: this.documentSelected.id,
        step:1,
        fileType:this.documentSelected.fileType,
        base64Cert:event.detail.cert,
        reason:"fuck",
        location:'hanoi'
      };
      
      documentService.sign(obj).subscribe(res => {
        console.log("Res");
        console.log(res);
        signToken = res.body.token;
        console.log("signToken " + signToken);
        let data = {
          hash :res.body.data ,
          cert:event.detail.cert,
          fileType: this.documentSelected.fileType
        };
        console.log(data);
        document.dispatchEvent(new CustomEvent('signHashEvt',{detail:data}));
      }
      )
    });
    document.addEventListener("signHashResultEvent",(event:any) => {
      console.log("signHashResultEvent");
      console.log(event);
      let obj = {
        id: this.documentSelected.id,
        step:3,
        fileType:this.documentSelected.fileType,
        signature:event.detail.signedHash
      };
      const headers = new HttpHeaders().set('X-SIGN-TOKEN',signToken);
      documentService.sign(obj,headers).subscribe(res => {
        console.log("Res");
        console.log(res);
      }
      )
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  loadAll():void {
    this.documentService.query().subscribe(
      (res: HttpResponse<IDocument[]>) => {
        this.documents = res.body ?? [];
      },
      () => {
          //
      }
    );
  }

  onSign(documentValue: IDocument) {
    console.log("document");
    this.documentSelected = documentValue;
    document.dispatchEvent(new CustomEvent('selectCertEvt'));
    // let req = {
    //     id:document.id,
    //     step:1,
    //     fileType:document.fileType,
    // }
  }


}
