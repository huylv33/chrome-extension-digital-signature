import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DocumentComponent } from './document/document.component';
import { SigningComponent } from './signing/signing.component';

const routes: Routes = [
  { path: 'signing', component: SigningComponent },
  { path: 'documents', component: DocumentComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
