package com.nms.app.exception;

public class SignException extends Exception {
    public SignException() {
    }

    public SignException(String message) {
        super(message);
    }

    public SignException(Throwable cause) {
        super(cause);
    }

    public SignException(String message, Throwable cause) {
        super(message, cause);
    }
}
