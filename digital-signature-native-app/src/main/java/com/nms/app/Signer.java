package com.nms.app;

import com.nms.app.middleware.MiddlewareChrome;
import com.nms.app.utils.digitalsignature.SignatureUtils;
import com.nms.app.utils.digitalsignature.ooxml.OOXMLDigitalSign;
import com.nms.app.utils.digitalsignature.pdf.PDFDigitalSign;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;

public class Signer {

	private static final String CLASS_NAME = Signer.class.getName();

	public static void main(String[] args){
		MiddlewareChrome.log(CLASS_NAME, "------------ Start Main --------------");
		if (args.length == 0 || !args[0].equals("ChromeExtensionMessage")){
			return;
		}
		MiddlewareChrome.log(CLASS_NAME, "Connect with Chrome extension");
		MiddlewareChrome middlewareChrome = MiddlewareChrome.getInstance();
		JSONObject extensionMess =  middlewareChrome.readMessage();
		MiddlewareChrome.log(CLASS_NAME,extensionMess.toString());

		if (middlewareChrome.getRequestedAction().equals(MiddlewareChrome.ACTION_SIGN)) {
			MiddlewareChrome.log(CLASS_NAME,"check mess "+ extensionMess.getString("filename"));
			String input = extensionMess.getString("filename");
			try {
				var output = input.substring(0, input.length() - ".pdf".length()) + "_signed.pdf";
				MiddlewareChrome.log(CLASS_NAME, "Before sign");
				PDFDigitalSign.signPDF(input,output);
				MiddlewareChrome.log(CLASS_NAME, "after sign");
				middlewareChrome.sendMessage("{\"native_app_message\": \"end\"}");
				MiddlewareChrome.log(CLASS_NAME,"{\"native_app_message\": \"end\"," +
						"\"local_path_newFile\": \"" + output + "\"" + "}");
				MiddlewareChrome.log(CLASS_NAME, "after send message");
			} catch (GeneralSecurityException | IOException e) {
				MiddlewareChrome.log(CLASS_NAME,e.toString());
				middlewareChrome.sendError("Có lỗi trong quá trình ký");
			}
		}
		else if (middlewareChrome.getRequestedAction().equals("selectCert")) {
			MiddlewareChrome.log(CLASS_NAME, "select cert");
			X509Certificate x509Certificate ;
			try {
				x509Certificate = SignatureUtils.selectCertificate();
				String base64Str = "";
				if (x509Certificate != null) {
					base64Str = Base64.getEncoder().encodeToString(x509Certificate.getEncoded());
					middlewareChrome.sendMessage("{\"native_app_message\": \"select_cert_end\",\"cert\": \"" + base64Str + "\"}");
				}
			} catch (CertificateException | IOException e) {
				e.printStackTrace();
			}
		} else if (middlewareChrome.getRequestedAction().equals("signHash")) {
			MiddlewareChrome.log(CLASS_NAME, "sign hash");
			KeyStore ks = null;
			try {
				String base64Cert = extensionMess.getString("cert");
				CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
				InputStream in = new ByteArrayInputStream(Base64.getDecoder().decode(base64Cert));
				X509Certificate x509Certificate = (X509Certificate)certFactory.generateCertificate(in);
				ks = KeyStore.getInstance("Windows-MY", "SunMSCAPI");
				ks.load(null);
				String alias = ks.getCertificateAlias(x509Certificate);
				PrivateKey key = (PrivateKey) ks.getKey(alias, null);
				String hash = extensionMess.getString("hash");
				String base64Sig = "";
				String fileType = extensionMess.getString("fileType");
				if (fileType.equals("pdf")) {
					byte[] sig = new PDFDigitalSign().signHash(Base64.getDecoder().decode(hash),key);
					base64Sig = Base64.getEncoder().encodeToString(sig);
				} else if (fileType.equals("xlsx")) {
					byte[] sig = new OOXMLDigitalSign().signHash(Base64.getDecoder().decode(hash),key);
					base64Sig = Base64.getEncoder().encodeToString(sig);
				}
				middlewareChrome.sendMessage("{\"native_app_message\": \"sign_hash_end\",\"signedHash\": \"" + base64Sig + "\"}");
				MiddlewareChrome.log(CLASS_NAME,  base64Sig);
			} catch (Exception e) {
				MiddlewareChrome.log(CLASS_NAME, e.getMessage());
				middlewareChrome.sendError("Có lỗi trong quá trình ký");
			}

		}
	}

}
