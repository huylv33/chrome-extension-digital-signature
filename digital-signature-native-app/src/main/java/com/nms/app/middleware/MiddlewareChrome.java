package com.nms.app.middleware;

import org.json.JSONObject;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MiddlewareChrome {
	public static final String ACTION_SIGN = "sign";
	public static final String ACTION_NULL = "null";

	private enum JSONKey {
		ACTION("action");
		private final String keyName;

		JSONKey(String s) {
			keyName = s;
		}

		public String toString() {
			return this.keyName;
		}
	}

	// ****** ****** ****** ******

	private static final String LOG_FILE_NAME = "SignerMiddlewareLog.txt";
	private final String className = this.getClass().getName();
	private JSONObject jsonObject = null;

	// ** Singleton **

	private static MiddlewareChrome instance = null;

	private MiddlewareChrome() {
		log(className, "Start");
	}
	
	/**
	 * Return the singleton instance of MiddlewareChrome
	 * @return the instance
	 */
	public static MiddlewareChrome getInstance() {
		if (instance == null)
			instance = new MiddlewareChrome();
		return instance;
	}

	/**
	 * Read message from Chrome Extension and  fill a json object in the middleware.
	 */
	public JSONObject readMessage() {
		log(className, "Read Message");
		try {
			String jsonIn = readMessage(System.in);
			log(className, "Host received " + jsonIn);
			jsonObject = new JSONObject(jsonIn);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jsonObject;
	}

	/**
	 * Get the action to perform. The action request arrive from the Chrome Extension.
	 * The allowed values are:
	 * <ul>
	 * <ul>
	 * <li>ACTION_SIGN = "sign"</li>
	 * <li>ACTION_NULL = "null"</li>
	 * </ul>
	 * @return a string that identify the action
	 */
	public String getRequestedAction() {
		if (jsonObject.has(JSONKey.ACTION.toString())) {
			String requestAction = jsonObject.get(JSONKey.ACTION.toString()).toString();
			return requestAction;
		}
		return ACTION_NULL;
	}

	/**
	 * Read message from extension
	 * @param in
	 * @return
	 * @throws IOException
	 */
	private String readMessage(InputStream in) throws IOException {
		byte[] b = new byte[4];
		in.read(b);

		int size = getInt(b);

		if (size == 0) {
			log(className, "Blocked communication");
//			throw new InterruptedIOException("Blocked communication");
		}

		b = new byte[size];
		in.read(b);

		return new String(b, "UTF-8");
	}

	/**
	 * @param message
	 * @throws IOException
	 */
	public void sendMessage(String message) throws IOException {
		System.out.write(getBytes(message.length()));
		System.out.write(message.getBytes("UTF-8"));
		System.out.flush();
	}

	/**
	 * Send an error message to the Chrome Extension
	 * @param message message of error to send
	 */
	public void sendError(String message) {
		try {
			JSONObject jo = new JSONObject();
			jo.put("native_app_message", "error");
			jo.put("error", message);
			getInstance().sendMessage(jo.toString());
			System.exit(0);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private int getInt(byte[] bytes) {
		return (bytes[3] << 24) & 0xff000000 | (bytes[2] << 16) & 0x00ff0000 | (bytes[1] << 8) & 0x0000ff00
				| (bytes[0] << 0) & 0x000000ff;
	}

	private byte[] getBytes(int length) {
		byte[] bytes = new byte[4];
		bytes[0] = (byte) (length & 0xFF);
		bytes[1] = (byte) ((length >> 8) & 0xFF);
		bytes[2] = (byte) ((length >> 16) & 0xFF);
		bytes[3] = (byte) ((length >> 24) & 0xFF);
		return bytes;
	}

	/**
	 * Write to log file.
	 * @param sender identify who write message.
	 * @param message
	 */
	public static void log(String sender, String message) {
		File file = new File(System.getProperty("user.dir"), LOG_FILE_NAME);
		try {
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fileWriter = new FileWriter(file.getAbsoluteFile(), true);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();

			bufferedWriter.write(dateFormat.format(date) + ", " + sender + ": " + message + "\r\n");
			bufferedWriter.close();
		} catch (Exception e) {
			//
		}
	}

}
