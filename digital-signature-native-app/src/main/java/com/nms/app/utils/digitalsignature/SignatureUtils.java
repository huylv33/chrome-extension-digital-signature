package com.nms.app.utils.digitalsignature;

import com.sun.jna.platform.win32.Crypt32;
import com.sun.jna.platform.win32.Cryptui;

import java.io.ByteArrayInputStream;
import java.security.PrivateKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;

public class SignatureUtils {

    public static boolean isMSCapi(final PrivateKey key) {
        return key != null && key.getClass().getName().contains("mscapi");
    }

    public static X509Certificate selectCertificate() throws CertificateException {
        var handle = Crypt32.INSTANCE.CertOpenSystemStore(null, "MY");
        try {
            var certCtx = Cryptui.INSTANCE.CryptUIDlgSelectCertificateFromStore(handle,
                    null, null, null, 16, 0, null);
            if (certCtx != null) {
                try {
                    CertificateFactory fac = CertificateFactory.getInstance("X.509");
                    X509Certificate cert = (X509Certificate) fac.generateCertificate(
                            new ByteArrayInputStream(certCtx.pbCertEncoded.getByteArray(0, certCtx.cbCertEncoded)));
                    return cert;
                } finally {
                    Crypt32.INSTANCE.CertFreeCertificateContext(certCtx);
                }
            }
        } finally {
            if (handle != null)
                Crypt32.INSTANCE.CertCloseStore(handle, 0);
        }
        return null;
    }

//    public static void main(String[] args) throws CertificateException {
//        X509Certificate x509Certificate = selectCertificate();
//        String base64Str = Base64.getEncoder().encodeToString(x509Certificate.getEncoded());
//    }

}
