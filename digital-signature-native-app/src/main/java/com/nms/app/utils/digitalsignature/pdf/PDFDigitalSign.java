package com.nms.app.utils.digitalsignature.pdf;

import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.StampingProperties;
import com.itextpdf.signatures.*;
import com.nms.app.utils.digitalsignature.SignatureUtils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;

public class PDFDigitalSign {

    public static void signPDF(String input,String output) throws GeneralSecurityException
            , IOException {
        X509Certificate cert = SignatureUtils.selectCertificate();
        var ks = KeyStore.getInstance("Windows-MY", "SunMSCAPI");
        ks.load(null);
        String alias = ks.getCertificateAlias(cert);
        Certificate[] chain = ks.getCertificateChain(alias);
        PrivateKey key = (PrivateKey) ks.getKey(alias, null);
        PdfSigner signer;
        try (var reader = new PdfReader(input)) {
            signer = new PdfSigner(reader, new FileOutputStream(output), new StampingProperties());
            IExternalSignature pks = new PrivateKeySignature(key, DigestAlgorithms.SHA256, ks.getProvider().getName());
            IExternalDigest digest = new BouncyCastleDigest();
            signer.signDetached(digest,pks,chain,null,null,null,0,PdfSigner.CryptoStandard.CMS);
        }
    }

    public byte[] signHash(byte[] hash, PrivateKey pk) throws GeneralSecurityException, IOException {
        if (hash == null) {
            throw new NullPointerException("hash digest is not null");
        } else if (pk == null) {
            throw new NullPointerException("private key is not null");
        }
        String signMode = DigestAlgorithms.getDigest(DigestAlgorithms.getAllowedDigest(DigestAlgorithms.SHA256)) + "with" + pk.getAlgorithm();
        Signature sig;
        final String provider = SignatureUtils.isMSCapi(pk) ? "SunMSCAPI" : "SunRsaSign";
        if (Security.getProvider(provider) != null) {
            sig = Signature.getInstance(signMode, provider);
        } else {
            sig = Signature.getInstance(signMode);
        }
        sig.initSign(pk);
        sig.update(hash);
        return sig.sign();
    }

//    public static void main(String[] args) throws GeneralSecurityException, IOException {
//        X509Certificate cert = SignatureUtils.selectCertificate();
//        var ks = KeyStore.getInstance("Windows-MY", "SunMSCAPI");
//        ks.load(null);
//        String alias = ks.getCertificateAlias(cert);
//        Certificate[] chain = ks.getCertificateChain(alias);
//        PrivateKey key = (PrivateKey) ks.getKey(alias, null);
//        String hash = "MUswGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAvBgkqhkiG9w0BCQQxIgQgSbEhyLE+4BpbG0nH9EXx/wvP9TtIWRpZv2E9zNosm94=";
//        byte[] sig = new PDFDigitalSign().signHash(Base64.getDecoder().decode(hash),key);
//        String base64Sig = Base64.getEncoder().encodeToString(sig);
//    }
}
