package com.nms.app.utils.digitalsignature.ooxml;

import com.nms.app.exception.SignException;
import com.nms.app.utils.digitalsignature.SignatureUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.crypt.HashAlgorithm;
import org.apache.poi.poifs.crypt.dsig.SignatureConfig;
import org.apache.poi.poifs.crypt.dsig.SignatureInfo;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Collections;

public class OOXMLDigitalSign extends SignatureInfo  {

    public static void signOOXML(String input, String output) throws GeneralSecurityException, IOException, MarshalException,
            XMLSignatureException, SignException {
        X509Certificate cert = SignatureUtils.selectCertificate();
        var ks = KeyStore.getInstance("Windows-MY", "SunMSCAPI");
        ks.load(null);
        String alias = ks.getCertificateAlias(cert);
        PrivateKey key = (PrivateKey) ks.getKey(alias, null);
        SignatureConfig sc = new SignatureConfig();
        sc.setAllowMultipleSignatures(true);
        sc.setKey(key);
        sc.setSigningCertificateChain(Collections.singletonList(cert));
        SignatureInfo si = new SignatureInfo();
        si.setSignatureConfig(sc);
        // Load file to stream to avoid overwrite old file automatically
        try (var bais = new ByteArrayInputStream(java.nio.file.Files.readAllBytes(Path.of(input)))) {
            try (OPCPackage pkg = OPCPackage.open(bais)) {
                if (si.getSignatureConfig().getKey() != null) {
                    si.setOpcPackage(pkg);
                    si.confirmSignature();
                    pkg.save(new File(output));
                } else {
                    throw new SignException("Không có khóa bí mật");
                }
            }
        } catch (IOException | InvalidFormatException e) {
            throw new SignException("Không mở hoặc lưu được file, kiểm tra lại đường dẫn"
                    + " hoặc file đang được mở bởi ứng dụng khác");
        }
    }

    public byte[] signHash(byte[] hash, PrivateKey privateKey) throws GeneralSecurityException, IOException {
        try (final DigestOutputStream dos = getDigestStream(HashAlgorithm.sha256, privateKey)) {
            dos.init();
            dos.write(hash,0,hash.length);
            return dos.sign();
        }
    }

    private static DigestOutputStream getDigestStream(final HashAlgorithm algo, final PrivateKey key) {
        switch (algo) {
            case md2: case md5: case sha1: case sha256: case sha384: case sha512:
                return new SignatureOutputStream(algo, key);
            default:
                return new DigestOutputStream(algo, key);
        }
    }
}
