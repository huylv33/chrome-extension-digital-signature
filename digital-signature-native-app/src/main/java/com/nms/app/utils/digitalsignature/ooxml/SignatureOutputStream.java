package com.nms.app.utils.digitalsignature.ooxml;

import com.nms.app.utils.digitalsignature.SignatureUtils;
import org.apache.poi.poifs.crypt.HashAlgorithm;

import java.io.IOException;
import java.security.*;

/* package */ class SignatureOutputStream extends DigestOutputStream {
    Signature signature;

    SignatureOutputStream(final HashAlgorithm algo, PrivateKey key) {
        super(algo, key);
    }

    @Override
    public void init() throws GeneralSecurityException {
        final String provider = SignatureUtils.isMSCapi(key) ? "SunMSCAPI" : "SunRsaSign";
        if (Security.getProvider(provider) != null) {
            signature = Signature.getInstance(algo.ecmaString + "withRSA", provider);
        } else {
            signature = Signature.getInstance(algo.ecmaString + "withRSA");
        }

        signature.initSign(key);
    }

    @Override
    public byte[] sign() throws SignatureException {
        return signature.sign();
    }


    @Override
    public void write(final int b) throws IOException {
        try {
            signature.update((byte)b);
        } catch (final SignatureException e) {
            throw new IOException(e);
        }
    }

    @Override
    public void write(final byte[] data, final int off, final int len) throws IOException {
        try {
            signature.update(data, off, len);
        } catch (final SignatureException e) {
            throw new IOException(e);
        }
    }
}
