package com.nms.signing.utils.digitalsignature.pdf;

import com.itextpdf.io.image.ImageData;
import com.itextpdf.kernel.exceptions.PdfException;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.*;
import com.itextpdf.signatures.*;
import com.nms.signing.utils.digitalsignature.CertUtils;
import com.nms.signing.utils.digitalsignature.SignatureUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.Signature;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.*;

public class PDFUsbDigitalSign extends PdfSigner implements Serializable {

//    private static final Logger LOGGER = LoggerFactory.getLogger(PDFUsbDigitalSign.class);
    private String originFilePath;
    private String outputFilePath;
    private int estimatedSize = 0;
    private byte[] hash;
    private CryptoStandard sigType;
    private IExternalDigest externalDigest;
    private Collection<byte[]> crlBytes;
    private Collection<ICrlClient> crlList = new ArrayList<>();
    private IOcspClient ocspClient;
    private ITSAClient tsaClient;
    private PdfPKCS7 pdfPKCS7;
    private PdfName digestMethod;
    private List<byte[]> ocspList;
    private String hashAlgo = DigestAlgorithms.SHA256;
    private boolean visibleSignature = false;
    private Rectangle rectangle;
    private ImageData signatureImage;
    private PdfSignatureAppearance.RenderingMode renderingMode;

    public PDFUsbDigitalSign(String originFilePath, String outputFilePath) throws IOException {
        super(new PdfReader(originFilePath), new FileOutputStream(outputFilePath), new StampingProperties().useAppendMode());
        this.externalDigest = new BouncyCastleDigest();
    }

    public String getOriginFilePath() {
        return originFilePath;
    }

    public void setOriginFilePath(String originFilePath) {
        this.originFilePath = originFilePath;
    }

    public String getOutputFilePath() {
        return outputFilePath;
    }

    public void setOutputFilePath(String outputFilePath) {
        this.outputFilePath = outputFilePath;
    }

    public Collection<ICrlClient> getCrlList() {
        return crlList;
    }

    public void setCrlList(Collection<ICrlClient> crlList) {
        this.crlList = crlList;
    }

    public IOcspClient getOcspClient() {
        return ocspClient;
    }

    public void setOcspClient(IOcspClient ocspClient) {
        this.ocspClient = ocspClient;
    }

    public ITSAClient getTsaClient() {
        return tsaClient;
    }

    public void setTsaClient(ITSAClient tsaClient) {
        this.tsaClient = tsaClient;
    }

    public String getHashAlgo() {
        return hashAlgo;
    }

    public void setHashAlgo(String hashAlgo) {
        this.hashAlgo = hashAlgo;
    }

    public boolean isVisibleSignature() {
        return visibleSignature;
    }

    public void setVisibleSignature(boolean visibleSignature) {
        this.visibleSignature = visibleSignature;
    }

    public Rectangle getRectangle() {
        return rectangle;
    }

    public void setRectangle(Rectangle rectangle) {
        this.rectangle = rectangle;
    }

    public ImageData getSignatureImage() {
        return signatureImage;
    }

    public void setSignatureImage(ImageData signatureImage) {
        this.signatureImage = signatureImage;
    }

    public PdfSignatureAppearance.RenderingMode getRenderingMode() {
        return renderingMode;
    }

    public void setRenderingMode(PdfSignatureAppearance.RenderingMode renderingMode) {
        this.renderingMode = renderingMode;
    }

    public byte[] getDigest(String base64Cert, String reason, String location) throws IOException, GeneralSecurityException {
//        LOGGER.info("getDigest: ------------begin-----------");
        byte[] result;
        X509Certificate x509Cert = CertUtils.getX509Cert(base64Cert);
        Certificate[] chain = new Certificate[]{(Certificate) x509Cert};
        if (this.closed) {
            throw new PdfException("This instance of PdfSigner has been already closed.");
        } else if (this.certificationLevel > 0 && this.isDocumentPdf2() && this.documentContainsCertificationOrApprovalSignatures()) {
            throw new PdfException("Certification signature creation failed. Document shall not contain any certification or approval signatures before signing with certification signature.");
        } else {
            Collection<byte[]> crlBytes = null;

            for (int i = 0; crlBytes == null && i < chain.length; crlBytes = this.processCrl(chain[i++], crlList)) {
            }
            if (estimatedSize == 0) {
                estimatedSize = 8192;
                byte[] element;
                if (crlBytes != null) {
                    for (Iterator itr = crlBytes.iterator(); itr.hasNext(); estimatedSize += element.length + 10) {
                        element = (byte[]) itr.next();
                    }
                }

                if (ocspClient != null) {
                    estimatedSize += 4192;
                }

                if (tsaClient != null) {
                    estimatedSize += 4192;
                }
            }
            PdfSignatureAppearance appearance = this.getSignatureAppearance();
            appearance.setCertificate(chain[0]);
            appearance.setLocation(location);
            appearance.setReason(reason);
            if (visibleSignature) {
                appearance.setRenderingMode(renderingMode)
                        .setPageRect(rectangle);
                if (renderingMode == PdfSignatureAppearance.RenderingMode.GRAPHIC ||
                        renderingMode == PdfSignatureAppearance.RenderingMode.GRAPHIC_AND_DESCRIPTION) {
                    appearance.setSignatureGraphic(signatureImage);
                }
            }
            if (this.sigType == CryptoStandard.CADES && !this.isDocumentPdf2()) {
                this.addDeveloperExtension(PdfDeveloperExtension.ESIC_1_7_EXTENSIONLEVEL2);
            }

            PdfSignature dic = new PdfSignature(PdfName.Adobe_PPKLite, this.sigType == CryptoStandard.CADES ?
                    PdfName.ETSI_CAdES_DETACHED : PdfName.Adbe_pkcs7_detached);
            dic.setReason(appearance.getReason());
            dic.setLocation(appearance.getLocation());
            dic.setSignatureCreator(appearance.getSignatureCreator());
            dic.setContact(appearance.getContact());
            dic.setDate(new PdfDate(this.getSignDate()));
            this.cryptoDictionary = dic;
            this.digestMethod = this.getHashAlgorithmNameInCompatibleForPdfForm(this.hashAlgo);
            Map<PdfName, Integer> exc = new HashMap();
            exc.put(PdfName.Contents, estimatedSize * 2 + 2);
            this.preClose(exc);
            this.pdfPKCS7 = new PdfPKCS7(null, chain, this.hashAlgo, null, externalDigest, false);
            InputStream data = this.getRangeStream();
            this.hash = DigestAlgorithms.digest(data, this.externalDigest.getMessageDigest(this.hashAlgo));
            byte[] ocsp;
            if (chain.length > 1 && ocspClient != null) {
                for (int j = 0; j < chain.length - 1; ++j) {
                    ocsp = ocspClient.getEncoded((X509Certificate) chain[j], (X509Certificate) chain[j + 1], null);
                    if (ocsp != null) {
                        this.ocspList.add(ocsp);
                    }
                }
            }
            result = this.pdfPKCS7.getAuthenticatedAttributeBytes(hash, this.sigType, ocspList, crlBytes);
//            LOGGER.info("getDigest: ------------end-----------");
        }
        return result;
    }

    public boolean appendSignature(byte[] extSignature) {
        boolean result = false;
//        LOGGER.info("appendSignature: ---------------begin---------------");
        try {
            if (this.appearance != null && this.pdfPKCS7 != null && this.hash != null && extSignature != null && this.estimatedSize > 0) {
                this.pdfPKCS7.setExternalDigest(extSignature, null, "RSA");
                byte[] encodedSig = this.pdfPKCS7.getEncodedPKCS7(this.hash, this.sigType, this.tsaClient, this.ocspList, this.crlBytes);
                if (this.estimatedSize + 2 < encodedSig.length) {
//                    LOGGER.error("appendSignature: Not enough space");
                } else {
                    byte[] paddedSig = new byte[estimatedSize];
                    System.arraycopy(encodedSig, 0, paddedSig, 0, encodedSig.length);
                    PdfDictionary dic2 = new PdfDictionary();
                    dic2.put(PdfName.Contents, (new PdfString(paddedSig)).setHexWriting(true));
                    this.close(dic2);
                    this.closed = true;
                    result = true;
                }
            } else {
//                LOGGER.error("appendSignature: parameters fail");
            }
        } catch (Exception ex) {
//            LOGGER.error("appendSignature: " + ex.getMessage());
        }
//        LOGGER.info("appendSignature: ---------------end---------------");
        return result;
    }

    public byte[] signHash(byte[] hash, PrivateKey pk) throws GeneralSecurityException {
        if (hash == null) {
            throw new NullPointerException("hash digest is not null");
        } else if (pk == null) {
            throw new NullPointerException("private key is not null");
        }
        String signMode = DigestAlgorithms.getDigest(DigestAlgorithms.getAllowedDigest(hashAlgo)) + "with" + pk.getAlgorithm();
        Signature sig;
        final String provider = SignatureUtils.isMSCapi(pk) ? "SunMSCAPI" : "SunRsaSign";
        if (Security.getProvider(provider) != null) {
            sig = Signature.getInstance(signMode, provider);
        } else {
            sig = Signature.getInstance(signMode);
        }
        sig.initSign(pk);
        sig.update(hash);
        return sig.sign();
    }

    private boolean isDocumentPdf2() {
        return this.document.getPdfVersion().compareTo(PdfVersion.PDF_2_0) >= 0;
    }

    private PdfName getHashAlgorithmNameInCompatibleForPdfForm(String hashAlgorithm) {
        PdfName pdfCompatibleName = null;
        String hashAlgOid = DigestAlgorithms.getAllowedDigest(hashAlgorithm);
        if (hashAlgOid != null) {
            String hashAlgorithmNameInCompatibleForPdfForm = DigestAlgorithms.getDigest(hashAlgOid);
            if (hashAlgorithmNameInCompatibleForPdfForm != null) {
                pdfCompatibleName = new PdfName(hashAlgorithmNameInCompatibleForPdfForm);
            }
        }

        return pdfCompatibleName;
    }

}
