package com.nms.signing.utils.digitalsignature.ooxml;

import com.nms.signing.web.rest.errors.SignException;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.apache.jcp.xml.dsig.internal.dom.DOMReference;
import org.apache.jcp.xml.dsig.internal.dom.DOMSignedInfo;
import org.apache.jcp.xml.dsig.internal.dom.DOMXMLSignature;
import org.apache.poi.ooxml.util.DocumentHelper;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.poifs.crypt.HashAlgorithm;
import org.apache.poi.poifs.crypt.dsig.SignatureConfig;
import org.apache.poi.poifs.crypt.dsig.SignatureInfo;
import org.apache.poi.poifs.crypt.dsig.facets.SignatureFacet;
import org.apache.jcp.xml.dsig.internal.dom.DOMSubTreeData;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.XMLStructure;
import javax.xml.crypto.dsig.*;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import static org.apache.poi.poifs.crypt.dsig.facets.SignatureFacet.XML_DIGSIG_NS;

@Getter
@Setter
public class OOXMLUsbDigitalSign extends SignatureInfo implements Serializable {
    private HashAlgorithm hashAlgo = HashAlgorithm.sha256;
    private String originFilePath;
    private String outputFilePath;
    private DOMSignContext xmlSignContext;

    public OOXMLUsbDigitalSign(String originFilePath, String outputFilePath) throws SignException {
        this.originFilePath = originFilePath;
        File originFile = new File(originFilePath);
        if (!originFile.exists() || originFile.isDirectory()) {
            throw new SignException("Đường dẫn file cần ký không hợp lệ");
        }
        this.outputFilePath = outputFilePath;
    }

    public String getOriginFilePath() {
        return originFilePath;
    }

    public void setOriginFilePath(String originFilePath) {
        this.originFilePath = originFilePath;
    }

    public String getOutputFilePath() {
        return outputFilePath;
    }

    public void setOutputFilePath(String outputFilePath) {
        this.outputFilePath = outputFilePath;
    }

    public DOMSignContext getXmlSignContext() {
        return xmlSignContext;
    }

    public void setXmlSignContext(DOMSignContext xmlSignContext) {
        this.xmlSignContext = xmlSignContext;
    }

    public String getDigest(List<X509Certificate> chain) throws IOException, InvalidFormatException,
            MarshalException, XMLSignatureException, NoSuchAlgorithmException, TransformException {
        var signatureConfig = new SignatureConfig();
        signatureConfig.setSigningCertificateChain(chain);
        signatureConfig.setAllowMultipleSignatures(true);
        PrivateKey privateKey = new PrivateKey() {
            @Override
            public String getAlgorithm() {
                return null;
            }

            @Override
            public String getFormat() {
                return null;
            }

            @Override
            public byte[] getEncoded() {
                return new byte[0];
            }
        };
        signatureConfig.setKey(privateKey);
        this.setSignatureConfig(signatureConfig);
        Files.copy(Paths.get(originFilePath), Paths.get(outputFilePath), StandardCopyOption.REPLACE_EXISTING);
        var pkg = OPCPackage.open(outputFilePath, PackageAccess.READ_WRITE);
        this.setOpcPackage(pkg);
        this.initXmlProvider();
        final var document = DocumentHelper.createDocument();
        xmlSignContext = this.createXMLSignContext(document);
        final var signedInfo = this.preSign(xmlSignContext);
        ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
        final Document parent = (Document)xmlSignContext.getParent();
        final Element el = getDsigElement(parent, "SignedInfo");
        final DOMSubTreeData subTree = new DOMSubTreeData(el, true);
        signedInfo.getCanonicalizationMethod().transform(subTree, xmlSignContext, dataStream);
        byte[] hash = dataStream.toByteArray();
        return Base64.getEncoder().encodeToString(hash);
    }

    private Element getDsigElement(final Document document, final String localName) {
        NodeList sigValNl = document.getElementsByTagNameNS(XML_DIGSIG_NS, localName);
        if (sigValNl.getLength() == 1) {
            return (Element)sigValNl.item(0);
        }
        return null;
    }

    public boolean appendSignature(final byte[] signature) {
        try {
            this.postSign(xmlSignContext, Base64.getEncoder().encodeToString(signature));
            this.getOpcPackage().close();
        } catch (MarshalException | IOException e) {
            File outputFile = new File(outputFilePath);
            if (outputFile.exists() && !outputFile.isDirectory()) {
                outputFile.setWritable(true);
                outputFile.delete();
            }
            return false;
        }
        return true;
    }

    public byte[] signHash(byte[] hash, PrivateKey privateKey) throws GeneralSecurityException, IOException {
        try (final DigestOutputStream dos = getDigestStream(hashAlgo, privateKey)) {
            dos.init();
            dos.write(hash,0,hash.length);
            return dos.sign();
        }
    }

    public DOMSignedInfo preSign(final DOMSignContext domSignContext) throws XMLSignatureException, MarshalException {

        final Document document = (Document) xmlSignContext.getParent();

        registerEventListener(document);

        // Signature context construction.
        if (getUriDereferencer() != null) {
            xmlSignContext.setURIDereferencer(getUriDereferencer());
        }

        getSignatureConfig().getNamespacePrefixes().forEach(xmlSignContext::putNamespacePrefix);

        xmlSignContext.setDefaultNamespacePrefix("");

        /*
         * Add ds:References that come from signing client local files.
         */
        List<Reference> references = new ArrayList<>();

        /*
         * Invoke the signature facets.
         */
        List<XMLObject> objects = new ArrayList<>();
        for (SignatureFacet signatureFacet : getSignatureConfig().getSignatureFacets()) {
            signatureFacet.preSign(this, document, references, objects);
        }

        /*
         * ds:SignedInfo
         */
        SignedInfo signedInfo;
        try {
            SignatureMethod signatureMethod = getSignatureFactory().newSignatureMethod
                    (getSignatureConfig().getSignatureMethodUri(), null);
            CanonicalizationMethod canonicalizationMethod = getSignatureFactory()
                    .newCanonicalizationMethod(getSignatureConfig().getCanonicalizationMethod(),
                            (C14NMethodParameterSpec) null);
            signedInfo = getSignatureFactory().newSignedInfo(
                    canonicalizationMethod, signatureMethod, references);
        } catch (GeneralSecurityException e) {
            throw new XMLSignatureException(e);
        }

        /*
         * JSR105 ds:Signature creation
         */
        String signatureValueId = getSignatureConfig().getPackageSignatureId() + "-signature-value";
        javax.xml.crypto.dsig.XMLSignature xmlSignature = getSignatureFactory()
                .newXMLSignature(signedInfo, null, objects, getSignatureConfig().getPackageSignatureId(),
                        signatureValueId);

        /*
         * ds:Signature Marshalling.
         */
        DOMXMLSignature domXmlSignature = (DOMXMLSignature) xmlSignature;
        Node documentNode = document.getDocumentElement();
        if (documentNode == null) {
            documentNode = document;
        }
        domXmlSignature.marshal(documentNode, null, xmlSignContext);
        /*
         * Completion of undigested ds:References in the ds:Manifests.
         */
        for (XMLObject object : objects) {
            List<XMLStructure> objectContentList = object.getContent();
            for (XMLStructure objectContent : objectContentList) {
                if (!(objectContent instanceof Manifest)) {
                    continue;
                }
                Manifest manifest = (Manifest) objectContent;
                List<Reference> manifestReferences = manifest.getReferences();
                for (Reference manifestReference : manifestReferences) {
                    if (manifestReference.getDigestValue() != null) {
                        continue;
                    }

                    DOMReference manifestDOMReference = (DOMReference) manifestReference;
                    manifestDOMReference.digest(xmlSignContext);
                }
            }
        }

        /*
         * Completion of undigested ds:References.
         */
        List<Reference> signedInfoReferences = signedInfo.getReferences();
        for (Reference signedInfoReference : signedInfoReferences) {
            DOMReference domReference = (DOMReference) signedInfoReference;

            // ds:Reference with external digest value
            if (domReference.getDigestValue() != null) {
                continue;
            }

            domReference.digest(xmlSignContext);
        }

        return (DOMSignedInfo) signedInfo;
    }

    private static DigestOutputStream getDigestStream(final HashAlgorithm algo, final PrivateKey key) {
        switch (algo) {
            case md2: case md5: case sha1: case sha256: case sha384: case sha512:
                return new SignatureOutputStream(algo, key);
            default:
                return new DigestOutputStream(algo, key);
        }
    }
}
