package com.nms.signing.utils.digitalsignature;

import java.security.PrivateKey;

public class SignatureUtils {
    public static boolean isMSCapi(final PrivateKey key) {
        return key != null && key.getClass().getName().contains("mscapi");
    }
}

