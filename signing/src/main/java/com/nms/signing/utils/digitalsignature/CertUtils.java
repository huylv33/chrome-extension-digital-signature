package com.nms.signing.utils.digitalsignature;

import java.io.ByteArrayInputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;

public class CertUtils {

    public static X509Certificate getX509Cert(String certStr) {
        CertificateFactory cf;
        try {
            cf = CertificateFactory.getInstance("X.509");
            if (!certStr.isEmpty()) {
                return (X509Certificate)cf.generateCertificate(new ByteArrayInputStream(Base64.getDecoder().decode(certStr.getBytes())));
            }
        } catch (CertificateException e) {
            e.printStackTrace();
        }
        return null;
    }
}
