package com.nms.signing.utils;

import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

@Component
public class RedisUtils {
    private final RedisTemplate<String, Object> redisTemplate;
    private  HashOperations<String, String,Object> hashOperations;
    private final long NOT_EXPIRED = -1;
    private final long DEFAULT_EXPIRED = 1800;

    public RedisUtils(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
        this.hashOperations = redisTemplate.opsForHash();
    }

    public void set(String key, String hashKey, Object value,long expired){
        hashOperations.put(key,hashKey,value);
        if (expired != NOT_EXPIRED) {
            redisTemplate.expire(key,expired, TimeUnit.SECONDS);
        }
    }

    public void set(String key, String hashKey, Object value){
        set(key, hashKey, value,DEFAULT_EXPIRED);
    }

    public Object get(String key, String hashKey, long expire) {
        Object value = hashOperations.get(key,hashKey);
        if(expire != NOT_EXPIRED){
            redisTemplate.expire(key, expire, TimeUnit.SECONDS);
        }
        return value;
    }
     public Object get(String key, String hashKey) {
        return get(key,hashKey,NOT_EXPIRED);
     }
}
