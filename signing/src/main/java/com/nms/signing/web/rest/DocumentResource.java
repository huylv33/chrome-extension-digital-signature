package com.nms.signing.web.rest;

import com.nms.signing.domain.Document;
import com.nms.signing.service.DocumentService;
import com.nms.signing.service.dto.SignRequestDto;
import com.nms.signing.service.dto.SignResultDto;
import com.nms.signing.utils.RedisUtils;
import com.nms.signing.utils.digitalsignature.CertUtils;
import com.nms.signing.utils.digitalsignature.ooxml.OOXMLUsbDigitalSign;
import com.nms.signing.utils.digitalsignature.pdf.PDFUsbDigitalSign;
import com.nms.signing.web.rest.errors.SignException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.xml.crypto.dsig.TransformException;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@RestController
@RequestMapping("/api/documents")
public class DocumentResource {

    private final DocumentService documentService;
    private final RedisUtils redisUtils;

    public DocumentResource(DocumentService documentService, RedisUtils redisUtils) {
        this.documentService = documentService;
        this.redisUtils = redisUtils;
    }

    @GetMapping
    public List<Document> getAllDocuments() {
        return documentService.findAll();
    }

    @PostMapping
    public ResponseEntity<Document> createDocument(@RequestBody Document document) throws URISyntaxException {
        Document result = documentService.save(document);
        return ResponseEntity
                .created(new URI("/api/documents/" + result.getId()))
                .body(result);
    }

    @PostMapping("/sign")
    public ResponseEntity<SignResultDto> signDocument(HttpSession session, @RequestHeader HttpHeaders requestHeaders,
                                                      @RequestBody SignRequestDto signRequestDto)  {
        if (signRequestDto.getFileType().equals("pdf")) {
            if (signRequestDto.getStep() == 1) {
                Document document = documentService.findById(signRequestDto.getId()).orElse(null) ;
                if (document == null) {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
                }
                String originPath = document.getPath();
                File file = new File(originPath);
                if (!file.exists()) {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
                }
                String outputPath = originPath.substring(0,originPath.length() - ".pdf".length()) + "_signed.pdf";
                try {
                    PDFUsbDigitalSign pdfUsbDigitalSign = new PDFUsbDigitalSign(originPath,outputPath);
                    byte[] digest = pdfUsbDigitalSign.getDigest(signRequestDto.getBase64Cert(),signRequestDto.getReason(),signRequestDto.getLocation());
                    String base64Digest = Base64.getEncoder().encodeToString(digest);
//                    session.setAttribute(session.getId(), pdfUsbDigitalSign);
                    redisUtils.set("SignObject",session.getId(),pdfUsbDigitalSign);
                    HttpHeaders headers = new HttpHeaders();
                    headers.set("X-SIGN-TOKEN",session.getId());
//                    headers.set("Access-Control-Expose-Headers","X-SIGN-TOKEN");
                    SignResultDto signResultDto = new SignResultDto();
                    signResultDto.setData(base64Digest);
                    signResultDto.setToken(session.getId());
                    return ResponseEntity.ok().headers(headers).body(signResultDto);
                } catch (IOException | GeneralSecurityException e) {
                    e.printStackTrace();
                }
            } else if (signRequestDto.getStep() == 3) {
                PDFUsbDigitalSign pdfUsbDigitalSign = (PDFUsbDigitalSign) redisUtils.get("SignObject",requestHeaders.get("x-sign-token").get(0));
                if (pdfUsbDigitalSign != null) {
                    boolean result = pdfUsbDigitalSign.appendSignature(Base64.getDecoder().decode(signRequestDto.getSignature()));
                    if (result) {
                        SignResultDto signResultDto = new SignResultDto();
                        signResultDto.setData("success");
                        return ResponseEntity.ok().body(signResultDto);
                    }
                }
            }
        } else if (signRequestDto.getFileType().equals("xlsx")) {
            if (signRequestDto.getStep() == 1) {
                Document document = documentService.findById(signRequestDto.getId()).orElse(null) ;
                if (document == null) {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
                }
                String originPath = document.getPath();
                File file = new File(originPath);
                if (!file.exists()) {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
                }
                String outputPath = originPath.substring(0,originPath.length() - ".xlsx".length()) + "_signed.xlsx";
                try {
                    OOXMLUsbDigitalSign ooxmlUsbDigitalSign = new OOXMLUsbDigitalSign(originPath,outputPath);
                    X509Certificate x509Cert = CertUtils.getX509Cert(signRequestDto.getBase64Cert());
                    List<X509Certificate> chain = new ArrayList<>();
                    chain.add(x509Cert);
                    String base64Digest = ooxmlUsbDigitalSign.getDigest(chain);
                    redisUtils.set("SignOOXMLObject",session.getId(),ooxmlUsbDigitalSign);
                    HttpHeaders headers = new HttpHeaders();
                    headers.set("X-SIGN-TOKEN",session.getId());
//                    headers.set("Access-Control-Expose-Headers","X-SIGN-TOKEN");
                    SignResultDto signResultDto = new SignResultDto();
                    signResultDto.setData(base64Digest);
                    signResultDto.setToken(session.getId());
                    return ResponseEntity.ok().headers(headers).body(signResultDto);
                } catch (Exception e){
                    e.printStackTrace();
                }
            } else if (signRequestDto.getStep() == 3) {
                OOXMLUsbDigitalSign ooxmlUsbDigitalSign = (OOXMLUsbDigitalSign) redisUtils.get("SignOOXMLObject",requestHeaders.get("x-sign-token").get(0));
                if (ooxmlUsbDigitalSign != null) {
                    boolean result = ooxmlUsbDigitalSign.appendSignature(Base64.getDecoder().decode(signRequestDto.getSignature()));
                    if (result) {
                        SignResultDto signResultDto = new SignResultDto();
                        signResultDto.setData("success");
                        return ResponseEntity.ok().body(signResultDto);
                    }
                }
            }
        }
        return ResponseEntity.internalServerError().body(null);
    }
}
