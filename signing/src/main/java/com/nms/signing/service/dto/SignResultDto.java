package com.nms.signing.service.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class SignResultDto implements Serializable {
    String data;
    String token;
}
