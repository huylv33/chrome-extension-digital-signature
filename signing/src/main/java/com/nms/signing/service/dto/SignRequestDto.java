package com.nms.signing.service.dto;

import lombok.Data;

@Data
public class SignRequestDto {
    Long id;
    Integer step;
    String fileType;
    String base64Cert;
    String reason;
    String location;
    String signature;
}
