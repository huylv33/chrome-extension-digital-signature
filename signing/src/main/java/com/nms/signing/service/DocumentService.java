package com.nms.signing.service;

import com.nms.signing.domain.Document;

import java.util.List;
import java.util.Optional;

public interface DocumentService {
    List<Document> findAll();
    Optional<Document> findById(Long id);
    Document save(Document document);
}
