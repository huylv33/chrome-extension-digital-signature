console.log("Start background");

chrome.runtime.onInstalled.addListener(function () {
  chrome.declarativeContent.onPageChanged.removeRules(undefined, function () {
    chrome.declarativeContent.onPageChanged.addRules([{
      conditions: [
        new chrome.declarativeContent.PageStateMatcher({
          pageUrl: { urlSuffix: '.pdf' }
        }),
        new chrome.declarativeContent.PageStateMatcher({
          pageUrl: { urlSuffix: '.PDF' }
        }),
        // new chrome.declarativeContent.PageStateMatcher({
        //   pageUrl: { urlSuffix: '/signing' }
        // }),
        new chrome.declarativeContent.PageStateMatcher({
          pageUrl: { urlSuffix: '/documents' }
        }),
      ],
      actions: [new chrome.declarativeContent.ShowPageAction()]
    }]);
  });
}); 

const app = 'com.nms.app.signature.signer';

//port with native app
var nativeAppPort = null;

//possible value of appCurrentState
var StateEnum = {
  ready: "ready",
  downloadFile: "downloadFile",
  running: "running",
  signing: "signing",
  error: "error",
  complete: "complete"
};
Object.freeze(StateEnum)

//state of the app
var appCurrentState = StateEnum.start;

var storedSignatureData = {
  signatureData: "",
  localpath: "",

  empty: function () {
    this.signatureData = "";
    this.localPath = "";
  },

  isEmpty: function () {
    if (this.signatureData == "" && this.signatureData == "")
      return true;
    return false;
  }
}

/**
 * Open connection with native app and set message listeners.
 */
function openConnection() {
  nativeAppPort = chrome.runtime.connectNative(app);
  console.log("openConnection");
  console.log(nativeAppPort);
  nativeAppPort.onMessage.addListener(function (msg) {
    console.log("RECEIVED FROM NATIVE APP:");
    console.log(msg);
    if (msg.hasOwnProperty("native_app_message")) {
      if (msg.native_app_message == "end") {
        storedSignatureData.empty();
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) { // Fetch the current tab
          chrome.tabs.sendMessage(tabs[0].id,{
            state: "end",
            localPath: msg.local_path_newFile,
            isSuccess:true
          }, function (response) { });
        });
        appCurrentState = StateEnum.complete;
      } else if (msg.native_app_message == "error") {
        console.log("ERROR:" + msg.error);
        appCurrentState = StateEnum.error;
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) { // Fetch the current tab
          chrome.tabs.sendMessage(tabs[0].id,{
            state: 'error',
            error: msg.error
          }, function (response) { });
        });
      } else if (msg.native_app_message == "select_cert_end") {
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) { // Fetch the current tab
          chrome.tabs.sendMessage(tabs[0].id,{
            state: 'select_cert_end',
            cert: msg.cert
          }, function (response) { });
        });
      } else if (msg.native_app_message == "sign_hash_end") {
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) { // Fetch the current tab
          chrome.tabs.sendMessage(tabs[0].id,{
            state: 'sign_hash_end',
            signedHash: msg.signedHash
          }, function (response) { });
        });
    }}

  });

  nativeAppPort.onDisconnect.addListener(function () {
    console.log(chrome.runtime.lastError)
    console.log("Disconnected: " + chrome.runtime.lastError.message);
  });

  return nativeAppPort;
}

/**
 * Close connection with native app.
 */
function closeConnection() {
  nativeAppPort.disconnect();
}

/**
 * Download the pdf, get local path of downloaded file and call callback.
 * @param {string} pdfURL - url of the pdf
 * @param {*} data - signature data
 * @param {function(data):void} callback - callback
 */
function downloadFile(pdfURL, data, callback) {
  console.log("downloadFile");
  console.log("pdfUrl");
  console.log(pdfURL);
  console.log(data);
  appCurrentState = StateEnum.downloadFile;
  //1) get tab url
  downloadPDF(pdfURL)

  //2) download pdf 
  function downloadPDF(pdfUrl) {
    console.log("Start download document...")
    chrome.downloads.download({
      url: pdfUrl
    }, function (downloadItemID) {
      getLocalPath(downloadItemID);
    });
  }


  //3) get download file local path
  function getLocalPath(downloadItemID) {
    console.log("GET LOCAL PATH...")
    chrome.downloads.search({
      id: downloadItemID,
      state: "complete"
    }, function (item) {
      if (item.length == 0) {
        console.log("Downloading....");
        sleep(1500).then(() => { //wait X second
          getLocalPath(downloadItemID);
        });
      } else {
        console.log(item[0].filename);
        data.filename = item[0].filename;
        if (callback)
          callback(data)
      }
    });
  }

  // sleep time expects milliseconds
  function sleep(time) {
    return new Promise((resolve) => setTimeout(resolve, time));
  }
}

/**
 * Send data to native app for signing
 * @param {*} data - data to send to native app for signing 
 */
function sendDataForSign(data) {
  console.log("send data for sign")
  appCurrentState = StateEnum.signing;
  console.log("Send message to native app...")
  data.action = "sign";
  console.log(data);
  nativeAppPort.postMessage(data);
};


function selectCertInLocal(data) {
  console.log("select cert in local");
  data.action = signMessage.selectCert;
  console.log(data);
  nativeAppPort.postMessage(data);
}

function sendDataForSighHash(data) {
  console.log("send data for sign")
  console.log("Send message to native app...")
  data.action = "signHash";
  console.log(data);
  nativeAppPort.postMessage(data);
}

/**
 * types of message from the popup that background script can handle
 */
var popupMessageType = {
  wakeup: 'wakeup',
  init: 'init',
  disconnect: 'disconnect',
  download_and_sign: 'download_and_sign',
  sign: 'sign',
  resetState: "resetState"
}

//
var signMessage = {
	selectCert: "selectCert",
	signHash: "signHash"
}

//listener message Popup -> Background
chrome.runtime.onMessage.addListener(
  function (request, sender, sendResponse) {
    switch (request.action) {
      case popupMessageType.wakeup:
        console.log("Background wakeup");
        break;
      case popupMessageType.resetState:
        appCurrentState = StateEnum.start;
        sendResponse({
          appstate: appCurrentState
        })
        break;
      case popupMessageType.init:
        openConnection();
        break;
      case popupMessageType.disconnect:
        console.log("popupMessageType.disconnect");
        closeConnection();
        break;
      case popupMessageType.download_and_sign:
        downloadFile(request.url, request.data, sendDataForSign);
        break;
      case popupMessageType.sign: //used for directly sign a local file
        console.log("popupMessageType.sign");
        sendDataForSign(request.data);
        break;
      //
      case signMessage.selectCert:
        console.log("signMessage.selectCert");
        selectCertInLocal(request.data);
        break;
      case signMessage.signHash:
        console.log("signMessage.signHash");
        sendDataForSighHash(request.data)
      default:
        console.log("Invalid action");
        break;
    }
    sendResponse({
      ack: "success",
      received: request.action,
    });
  });