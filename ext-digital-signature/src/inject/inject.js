chrome.extension.sendMessage({}, function(response) {
	var readyStateCheckInterval = setInterval(function() {
	if (document.readyState === "complete") {
		clearInterval(readyStateCheckInterval);

		// ----------------------------------------------------------
		// This part of the script triggers when page is done loading
		console.log("Hello. This message was sent from scripts/inject.js");
		// ----------------------------------------------------------

	}
	}, 10);
});

var signatureData = {
    filename: "",
    tabUrl: "",
    
    empty: function () {
        this.filename = "";
        this.tabUrl = "";
    },

    copy: function (data) {
        this.filename = data.filename;
        this.tabUrl = data.tabUrl;
    }
};

/**
 * types of message from the popup that background script can handle
 */
 var popupMessageType = {
	wakeup: 'wakeup',
	init: 'init',
	disconnect: 'disconnect',
	download_and_sign: 'download_and_sign',
	sign: 'sign',
	resetState: "resetState"
}
  
document.addEventListener('signEvent', function(e){
	console.log(e);
	chrome.runtime.sendMessage({
		action: popupMessageType.init,
	}, function (response) {
		// console.log("<<< received:")
		// console.log(response);
	});
	chrome.runtime.sendMessage({
		// action: "download_and_sign",
		action: popupMessageType.download_and_sign,
		url: e.detail.fileUrl,
		data: signatureData
	}, function (response) {
		console.log("<<< received:")
		console.log(response.ack);
	});
 }, false);


chrome.runtime.onMessage.addListener(
	function (request, sender, sendResponse) {
		console.log("<<< In inject.js received from bg:")
		console.log(request);
		if (request.hasOwnProperty("state")) {
			switch (request.state) {
				case "end":
					let data = {
						type: 'Some data',
						isSuccess:request.isSuccess
					};
					console.log(data);  
					document.dispatchEvent(new CustomEvent('signResultEvent', {detail: data}));
					break;
				case "error":
					break;
				case "select_cert_end" : 
					document.dispatchEvent(new CustomEvent('selectCertResultEvent', {detail: {
						isSuccess: true,
						cert: request.cert
					}}));
					break;
				case "sign_hash_end" :
					document.dispatchEvent(new CustomEvent('signHashResultEvent',{detail :{
						signedHash: request.signedHash
					}}))
				default:
					break;
			}
		}
})

//////////

var signMessage = {
	selectCert: "selectCert",
	signHash: "signHash"
}

document.addEventListener("selectCertEvt", e => {
	console.log("selectCertEvt")
	console.log(e);
	chrome.runtime.sendMessage({
		action: popupMessageType.init,
	}, function (response) {
		// console.log("<<< received:")
		// console.log(response);
	});

	chrome.runtime.sendMessage({
		action: signMessage.selectCert,
		data:{}
	}, function (response) {
		console.log("<<< received:")
		console.log(response.ack);
	});
})


document.addEventListener("signHashEvt", e => {
	console.log("signHashEvt")
	console.log(e);
	chrome.runtime.sendMessage({
		action: popupMessageType.init,
	}, function (response) {
		// console.log("<<< received:")
		// console.log(response);
	});

	chrome.runtime.sendMessage({
		action: signMessage.signHash,
		data:e.detail
	}, function (response) {
		console.log("<<< received:")
		console.log(response.ack);
	});
})

