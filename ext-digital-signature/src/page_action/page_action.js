console.log("Start...")


//Object that include all needed data for signature process
var signatureData = {
    filename: "",
    tabUrl: "",
    
    empty: function () {
        this.filename = "";
        this.tabUrl = "";
    },

    copy: function (data) {
        this.filename = data.filename;
        this.tabUrl = data.tabUrl;
    }
};


class Sections {
    constructor() {
        this._section = {
                signSection: document.getElementById("sign-section"), //start
                signingSection: document.getElementById("signing-section"), //loading
                endSection: document.getElementById("success-section"), // success
                errorSection: document.getElementById("error-section") //error 
            },

            this._currentSection = this._section.signSection;
    }

    get section() {
        return this._section;
    }

    get currentSection() {
        return this._currentSection;
    }

    /**Set the current section with a section in _section property of the object */
    changeSection(nextSection) {
        for (const key in this._section) {
            if (this._section.hasOwnProperty(key)) {
                if (this._section[key] === nextSection) {
                    //hide "old" current section
                    this.hideCurrentSection();
                    this._currentSection = nextSection;
                    //show "old" current section
                    this._currentSection.classList.remove('d-none');

                    return;
                }
            }
        }
        console.error("changeSection: No valid section");
    }

    /**hide current visible section */
    hideCurrentSection() {
        this._currentSection.classList.add('d-none');
    };
}

document.addEventListener('DOMContentLoaded', function() {

    chrome.runtime.sendMessage({
        "action": "wakeup"
    }, function (res) {
        //variables related with background script
        const background = chrome.extension.getBackgroundPage();
        const popupMessageType = background.popupMessageType; //types of message from the popup that background script can handle
        const appStateEnum = background.StateEnum;
        const backgroundStoredSignatureData = background.storedSignatureData;
        let appCurrentState = background.appCurrentState;
        var sections = new Sections();
        // Buttons
        const signBtn = document.getElementById("sign-btn");
        const fileCreatedPath = document.getElementById("path-new-file");
        checkCurrentState();

        signBtn.addEventListener('click',function() {
            sections.changeSection(sections._section.signingSection);       
            getTabData(sign);
        });
        
        function checkCurrentState() {
            if (appCurrentState == undefined || appCurrentState == appStateEnum.complete || appCurrentState == appStateEnum.error) {
                clearData();
            }
            else if (appCurrentState == appStateEnum.signing || appCurrentState == appStateEnum.downloadFile) {
                sections.changeSection(sections.section.signingSection);
            }
            //check if exist stored data in background
            else if (appCurrentState == appStateEnum.running) {
                console.log(backgroundStoredSignatureData);
                if (backgroundStoredSignatureData.isEmpty() == false) {
                    console.log("NEED TO RESTORE DATA");
                    chrome.tabs.query({
                        active: true,
                        currentWindow: true
                    }, function (tab) {
                        if (tab[0].url == backgroundStoredSignatureData.signatureData.tabUrl) {
                            signatureData.copy(backgroundStoredSignatureData.signatureData);
                        } else {
                            console.log("Stored data in background belong to a different document. Clear stored data.")
                            clearData();
                        }
                    });
                }
            }
        }

        function clearData() {
            backgroundStoredSignatureData.empty();
            signatureData.empty();
            sections.changeSection(sections.section.signSection);
            fileCreatedPath.textContent = '';
            chrome.runtime.sendMessage({
                action: popupMessageType.resetState
            }, function (response) {
                appCurrentState = response.appstate;
            });
        }
        /** 
         * Get information about active browser tab.
         * Check if the file is local or you need to download it and after call a callback.
         * 
         *  @param {function(tabData):void} callback - A callback to run that that receive as input the tabData {location:"local"|"remote", url: "urlOfFile"}.
         *  tabData.url may be a local path on the machine or a URL
         */
        function getTabData(callback) {
            console.log("GET TAB URL...")
            let tabData = {};

            //check if file is already downloaded for get pdf info
            if (signatureData.filename != "" && !signatureData.filename.startsWith("http") && !signatureData.filename.startsWith("file")) {
                tabData.location = "local";
                tabData.url = signatureData.filename;
                if (callback)
                    callback(tabData);
            } else {
                chrome.tabs.query({
                    active: true,
                    currentWindow: true
                }, function (tab) {
                    let pdfURL = tab[0].url;
                    signatureData.tabUrl = tab[0].url;
                    console.log(pdfURL);
                    if (pdfURL.startsWith("file:///")) {
                        // file is local
                        pdfURL = pdfURL.substr("file:///".length);
                        console.log("File is local:");
                        console.log(pdfURL);
                        //restore all space that browser transform in
                        pdfURL = pdfURL.replace(/%20/g, ' ');
                        signatureData.filename = pdfURL;
                        tabData.location = "local";
                    } else {
                        tabData.location = "remote";
                    }
                    tabData.url = pdfURL;
                    console.log(tabData);
                    if (callback)
                        callback(tabData);
                });
            }
        }
        
        /**
         * Sign the pdf. This method usually is used as callback for 'getTabData' function.
         * If the file in the active tab is remote, before ask information download it.
         * @param {{location: "local" | "remote", url: string}} tabData - data of the tab
         */
         function sign(tabData) {
            chrome.runtime.sendMessage({
                action: popupMessageType.init,
            }, function (response) {
                // console.log("<<< received:")
                // console.log(response);
            });
            console.log("func sign in popup.js");
            console.log("tabdata");
            console.log(tabData);
            if (tabData.location == "remote") {
                // download pdf and then sign it
                chrome.runtime.sendMessage({
                    // action: "download_and_sign",
                    action: popupMessageType.download_and_sign,
                    url: tabData.url,
                    data: signatureData
                }, function (response) {
                    console.log("<<< received:")
                    console.log(response.ack);
                });
            } else if (tabData.location == "local") {
                chrome.runtime.sendMessage({
                    action: popupMessageType.sign,
                    data: signatureData
                }, function (response) {
                    console.log("<<< received:")
                    console.log(response.ack);
                });
            }
        }

        function showError(error) {
            sections.changeSection(sections.section.errorSection);
        }

        //listener message Background -> Popup
        // chrome.runtime.onMessage.addListener(
        //     function (request, sender, sendResponse) {
        //         console.log("<<< received:")
        //         console.log(request);
        //         if (request.hasOwnProperty("state")) {
        //             switch (request.state) {
        //                 case "end":
        //                     console.log(request.local_path_newFile);
        //                     fileCreatedPath.textContent = 'File created: ' + request.localPath;
        //                     sections.changeSection(sections.section.endSection);
        //                     break;
        //                 case "error":
        //                     showError(request.error);
        //                     break;
        //                 default:
        //                     break;
        //             }
        //         }
        // })
    })
})
